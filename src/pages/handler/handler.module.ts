import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HandlerPage } from './handler';

@NgModule({
  declarations: [
    HandlerPage,
  ],
  imports: [
    IonicPageModule.forChild(HandlerPage),
  ],
})
export class HandlerPageModule {}
