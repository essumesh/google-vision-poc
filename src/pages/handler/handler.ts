import { Component } from '@angular/core';
import { IonicPage, LoadingController, Loading } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';
import { tap, filter } from 'rxjs/operators';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import { AngularFirestore } from 'angularfire2/firestore';

import { Camera, CameraOptions } from '@ionic-native/camera';

import * as keyword_extractor from 'keyword-extractor';
import * as nearest_color from 'nearest-color';


@IonicPage()
@Component({
  selector: 'page-handler',
  templateUrl: 'handler.html',
})
export class HandlerPage {

  // Upload task
  task: AngularFireUploadTask;

  // Firestore data
  result$: Observable<any>;

  loading: Loading;
  image: string;
  inputPhrase: string;
  keys: Array<string> = [];
  labels: Array<string> = [];
  labelsCaptured: Array<string> = [];
  objectType: any;
  objectColor: any;
  objectBrand: any;
  body: any;
  docID: any;
  colors = {
    Black: '#000000',
    Navy: '#000075',
    Teal: '#469990',
    Olive: '#808000',
    Brown: '#9A6324',
    Maroon: '#800000',
    Red: '#e6194B',
    Orange: '#f58231',
    Yellow: '#ffe119',
    Lime: '#bfef45',
    Green: '#3cb44b',
    Cyan: '#42d4f4',
    Blue: '#4363d8',
    Purple: '#911eb4',
    Magenta: '#f032e6',
    Grey: '#a9a9a9',
    Pink: '#fabebe',
    Apricot: '#ffd8b1',
    Beige: '#fffac8',
    Mint: '#aaffc3',
    Lavender: '#e6beff',
    White: '#ffffff'
  };

  constructor(private storage: AngularFireStorage,
    private afs: AngularFirestore,
    private camera: Camera,
    private loadingCtrl: LoadingController,
    private http: HttpClient) {

    this.loading = this.loadingCtrl.create({
      content: 'Processing...'
    });

  }

  processNLP() {
    // let sentence = 'lost 2 backpacks and one black trolly and another white backpack';
    this.keys = keyword_extractor.extract(this.inputPhrase, {
      language: "english",
      remove_digits: true,
      return_changed_case: false,
      remove_duplicates: true

    });
    //  console.log(keys);
  }


  startUpload(file: string) {

    // Show loader
    this.loading = this.loadingCtrl.create({
      content: 'Processing...'
    });
    this.loading.present();

    // const timestamp = new Date().getTime().toString();
    const docId = this.afs.createId();
    this.docID = docId;
    const path = `${docId}.jpg`;

    // Make a reference to the future location of the firestore document
    const photoRef = this.afs.collection('photos').doc(docId)

    // Firestore observable
    this.result$ = photoRef.valueChanges()
      .pipe(
        filter(data => !!data),
        tap(_ => this.loading.dismiss())
    );

    this.result$.subscribe(items => {
      console.log(items);
      items.results[0].labelAnnotations.forEach(element => {
        this.labels.push(element.description);
      });
      this.objectType = this.processObjectType(items.results[0]);
      this.objectColor = this.processObjectColor(items.results[0]);
      this.objectBrand = this.processObjectBrand(items.results[0]);
      this.labelsCaptured.push(this.objectType);
      this.labelsCaptured.push(this.objectColor);
      this.labelsCaptured.push(this.objectBrand);
      let dataToSend = {
        object_type: this.objectType ? this.objectType.toLowerCase() : this.objectType,
        color: this.objectColor ? this.objectColor.toLowerCase() : this.objectColor,
        brand: this.objectBrand ? this.objectBrand.toLowerCase() : this.objectBrand,
        image_id: this.docID
      }
      this.body = JSON.stringify(dataToSend);
    });


    // The main task
    this.image = 'data:image/jpg;base64,' + file;
    this.task = this.storage.ref(path).putString(this.image, 'data_url');
  }

  submitClicked() {
    this.loading = this.loadingCtrl.create({
      content: 'Processing...'
    });
    this.loading.present();
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.http.post('https://xxx.xxx/addDataToCollection/?collectionName=photos-new',
    this.body,{ headers: headers }).subscribe(response => {
      console.log(response);
      this.resetData();
      this.loading.dismiss();
    },
    error => {
      this.resetData(); 
      this.loading.dismiss();
    });
  }
  
  resetData() {
    this.image = null;
    this.labelsCaptured = [];
  }

  processObjectType(resultsObj: any){
    return resultsObj.webDetection.bestGuessLabels.length > 0 ? resultsObj.webDetection.bestGuessLabels[0].label :
      this.labels[0];
  }

  processObjectColor(resultsObj: any){
    let colorRGB = resultsObj.imagePropertiesAnnotation.dominantColors.colors[0].color;
    return this.nearestColorMatch(colorRGB.red, colorRGB.green, colorRGB.blue);
  }

  processObjectBrand(resultsObj: any){
    return resultsObj.textAnnotations.length > 0 ? resultsObj.textAnnotations[0].description :
    resultsObj.logoAnnotations.length >  0 ? resultsObj.logoAnnotations[0].description : 'empty';
  }

  componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

  rgbToHex(r, g, b) {
    return "#" + this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b);
  }

  nearestColorMatch(r, g, b) {
    let colorCodeInHex = this.rgbToHex(r, g, b);
    // console.log(colorCodeInHex);
    let nearestColor = nearest_color.from(this.colors);
    return nearestColor(colorCodeInHex).name;
  }


  removeKey(indexToRemove: number) {
    this.labelsCaptured.slice(indexToRemove, 1);
  }

  // Gets the pic from the native camera then starts the upload
  async captureAndUpload() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
    }

    const base64 = await this.camera.getPicture(options);
    this.resetData();
    this.startUpload(base64);
  }

}
