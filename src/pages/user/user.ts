import { Component } from '@angular/core';
import { LoadingController, Loading } from 'ionic-angular';


import * as keyword_extractor from 'keyword-extractor';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFireStorage } from 'angularfire2/storage';

@Component({
  selector: 'page-user',
  templateUrl: 'user.html'
})
export class UserPage {

  loading: Loading;
  inputPhrase: string;
  keys: Array<string> = [];
  test: any;
  objectType: any;
  objectBrand: any;
  objectColor: any;
  objectTypeText: any;
  objectBrandText: any;
  objectColorText: any;
  body: any;
  username: any;
  imagesSRC: any;

  constructor(private storage: AngularFireStorage,
    private loadingCtrl: LoadingController,
    private http: HttpClient) {
    this.objectType = [
        {
          id: 1,
          key: 'bag'
        },
        {
          id: 2,
          key: 'mouse'
        },
        {
          id: 3,
          key: 'phone'
        },
        {
          id: 4,
          key: 'watch'
        }
      ];
    
    // this.objectBrand = [
    //     {
    //       id: 1,
    //       key: 'VIP'
    //     },
    //     {
    //       id: 2,
    //       key: 'AT'
    //     },
    //     {
    //       id: 3,
    //       key: 'Samsonite'
    //     }
    // ];

    this.objectColor = [
      {
        id: 1,
        key: 'Black'
      },
      {
        id: 2,
        key: 'Brown'
      },
      {
        id: 3,
        key: 'Navy'
      },
      {
        id: 4,
        key: 'Teal'
      },
      {
        id: 5,
        key: 'Olive'
      },
      {
        id: 6,
        key: 'Maroon'
      },
      {
        id: 7,
        key: 'Red'
      },
      {
        id: 8,
        key: 'Orange'
      },
      {
        id: 9,
        key: 'Yellow'
      },
      {
        id: 10,
        key: 'Lime'
      },
      {
        id: 11,
        key: 'Green'
      },
      {
        id: 12,
        key: 'Cyan'
      },
      {
        id: 13,
        key: 'Blue'
      },
      {
        id: 14,
        key: 'Purple'
      },
      {
        id: 15,
        key: 'Magenta'
      },
      {
        id: 16,
        key: 'Grey'
      },
      {
        id: 17,
        key: 'Pink'
      },
      {
        id: 19,
        key: 'Apricot'
      },
      {
        id: 20,
        key: 'Beige'
      },
      {
        id: 21,
        key: 'Mint'
      },
      {
        id: 22,
        key: 'Lavender'
      },
      {
        id: 23,
        key: 'White'
      }
    ];

    this.loading = this.loadingCtrl.create({
      content: 'Adding details to the database...'
    });

  }

  ionViewDidEnter() {
    this.resetData();
  }

  removeKey(indexToRemove: number) {
    this.keys.splice(indexToRemove, 1);
  }

  resetData() {
    this.objectBrandText = null;
    this.objectColorText = null;
    this.objectTypeText = null;
    this.username = null;
    this.body = null;
    this.imagesSRC = [];
  }

  submitClicked() {
    this.loading = this.loadingCtrl.create({
      content: 'Adding details to the database...'
    });
    let dataToSend = {
      username: this.username,
      object_type: this.objectTypeText ? this.objectTypeText.toLowerCase() : this.objectTypeText,
      color: this.objectColorText ? this.objectColorText.toLowerCase() : this.objectColorText,
      brand: this.objectBrandText ? this.objectBrandText.toLowerCase() : this.objectBrandText
    }
    this.body = JSON.stringify(dataToSend);
    // this.loading.present();
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.http.post('https://xxx.xxx/addDataToCollection/?collectionName=user-data',
    this.body, {headers: headers}).subscribe(response => {
      console.log(response);
      // this.loading.dismiss();
    });
  }

  processNLP() {
    // let sentence = 'lost 2 backpacks and one black trolly and another white backpack';
    this.keys = keyword_extractor.extract(this.inputPhrase, {
      language: "english",
      remove_digits: true,
      return_changed_case: false,
      remove_duplicates: true
    });
  }

  showRecommendation() {
    this.loading = this.loadingCtrl.create({
      content: 'Matching with image database...'
    });
    this.matchItem(0);
  }

  matchItem(i: number) {
    // use api to match user input on this line with the list of images
    // results are of the form ['doc_id','doc_id','doc_id']
     this.loading.present();
    // let headers = new HttpHeaders({ 'Accept': 'application/json' });
    this.http.get('https://xxx.xxx/matchUserTextWithImage?userInput=0,'
    + (this.objectTypeText ? this.objectTypeText.toLowerCase() : 'empty') + ',' + (this.objectBrandText ? this.objectBrandText.toLowerCase() : 'empty')
    + ',' + (this.objectColorText ? this.objectColorText.toLowerCase() : 'empty')).subscribe(response => {
      let imagesID = response;
      console.log(response);
      this.getImageIdsFromDocId(i, imagesID);
    });
    // this.getImageIdsFromDocId(['MZLxWIAuMWcufH23NJ1B']);

  }

  getImageIdsFromDocId(i: number, imagesID: any) {
    console.log(imagesID);
    if (!imagesID.response || imagesID.response.length <= 0) return;
    let listOfImageIDs = '';
    for (let item of imagesID.response) {
      console.log(item);
      if (listOfImageIDs !== '') listOfImageIDs = listOfImageIDs + ',';
      listOfImageIDs = listOfImageIDs + item;
    }
    console.log(listOfImageIDs);
    let headers = new HttpHeaders({ 'Accept': 'application/json' });
    this.http.get('https://xxx.xxx/getImageidsFromPhotosNew/?collectionIds='+listOfImageIDs,
    { headers: headers }).subscribe(response => {
      console.log(response);
      let imagesNameList = response;
      this.getDownloadURL(i, imagesNameList);
    });
}
  getDownloadURL(i: number, imagesNameList:any) {
    if (imagesNameList.length > 0) {
    this.imagesSRC = [];
    let j = 1;
    imagesNameList.forEach(element => {
    this.storage.ref(element + '.jpg').getMetadata().subscribe(item => {
      // console.log(item);
      this.imagesSRC.push(item.downloadURLs[0]);
      if (j === imagesNameList.length)
      this.loading.dismiss();
      j++;
    });
    });
  } else {
    this.loading.dismiss();
  }
}
}
