import { Component } from '@angular/core';

import { AdminPage } from '../admin/admin';
import { HandlerPage } from '../handler/handler';
import { UserPage } from '../user/user';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = UserPage;
  tab2Root = HandlerPage;
  tab3Root = AdminPage;

  constructor() {

  }
}
