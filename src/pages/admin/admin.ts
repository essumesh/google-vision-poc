import { Component } from '@angular/core';
import { LoadingController, Loading } from 'ionic-angular';
import { AngularFireStorage } from 'angularfire2/storage';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html'
})
export class AdminPage {

  loading: Loading;
  inputPhrase: string;
  keys: Array<string> = [];
  test: any;
  objectType: any;
  objectColor: any;
  objectBrand: any;
  lostItems: any;
  constructor(private storage: AngularFireStorage,
    private loadingCtrl: LoadingController,
    private http: HttpClient) {

    this.loading = this.loadingCtrl.create({
      content: 'Matching with image database...'
    });
    this.loadData();
    
  }

  loadData() {
    let headers = new HttpHeaders({ 'Accept': 'application/json' });
    this.http.get('https://xxx.xxx/getItemsFromUserDataCollection',
    { headers: headers }).subscribe(response => {
      console.log(response);
      this.lostItems = response;
      for (let it = 0; it < this.lostItems.length; it++) {
        this.lostItems[it].imagesSRC = [];
      }
      this.loading.dismiss();
    });
  }

  matchItem(i: number) {
    // use api to match user input on this line with the list of images
    // results are of the form ['doc_id','doc_id','doc_id']
     this.loading.present();
    // let headers = new HttpHeaders({ 'Accept': 'application/json' });
    this.http.get('xxx.xxx/matchUserTextWithImage?userInput=0,'
    + this.lostItems[i].object_type + ',' + this.lostItems[i].brand + ',' + this.lostItems[i].color).subscribe(response => {
      let imagesID = response;
      console.log(response);
      this.getImageIdsFromDocId(i, imagesID);
       this.loading.dismiss();
    });

  }

  getImageIdsFromDocId(i: number, imagesID: any) {
    console.log(imagesID);
    if (!imagesID.response || imagesID.response.length <= 0) return;
    let listOfImageIDs = '';
    for (let item of imagesID.response) {
      console.log(item);
      if (listOfImageIDs !== '') listOfImageIDs = listOfImageIDs + ',';
      listOfImageIDs = listOfImageIDs + item;
    }
    console.log(listOfImageIDs);
    let headers = new HttpHeaders({ 'Accept': 'application/json' });
    this.http.get('https://xxx.xxx/getImageidsFromPhotosNew/?collectionIds='+listOfImageIDs,
    { headers: headers }).subscribe(response => {
      console.log(response);
      let imagesNameList = response;
      this.getDownloadURL(i, imagesNameList);
    });
}
  getDownloadURL(i: number, imagesNameList:any) {
    let imagesSRC = [];
    this.lostItems[i].imagesSRC = [];
    imagesNameList.forEach(element => {
    this.storage.ref(element + '.jpg').getMetadata().subscribe(item => {
      imagesSRC.push(item.downloadURLs[0]);
      console.log(imagesSRC);
      this.lostItems[i].imagesSRC = imagesSRC
    });
    });
  }

  refreshData() {
    this.lostItems = [];
    this.loadData();
  }
 }
