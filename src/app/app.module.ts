import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { UserPage } from '../pages/user/user';
import { HandlerPage } from '../pages/handler/handler';
import { AdminPage } from '../pages/admin/admin';
import { TabsPage } from '../pages/tabs/tabs';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';

import { Camera } from '@ionic-native/camera';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AngularFireDatabaseModule } from 'angularfire2/database';


const firebaseConfig = {
  apiKey: "AIzaSyA52gmXLw6w7PHXHv7K2Q8IWykbIBIr854",
  authDomain: "lostandfoundapp-235306.firebaseapp.com",
  databaseURL: "https://cloudvision-234517.firebaseio.com",
  projectId: "lostandfoundapp-235306",
  storageBucket: "lostandfoundapp-235306.appspot.com",
  messagingSenderId: "1087155765911"
};

@NgModule({
  declarations: [
    MyApp,
    HandlerPage,
    UserPage,
    AdminPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    AngularFireStorageModule,
    HttpClientModule,
    AngularFireDatabaseModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HandlerPage,
    UserPage,
    AdminPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HttpClient,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Camera
  ]
})
export class AppModule {}
